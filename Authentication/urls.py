from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'story9'

urlpatterns = [
    path('', auth_views.LoginView.as_view(template_name="index.html"), name='index'),
    path('login/', auth_views.LoginView.as_view(template_name="login.html", redirect_authenticated_user=True), name="login"),
    path('logout/', auth_views.LogoutView.as_view(template_name="logout.html"), name="logout"),
    path('profile/', views.profile, name='profile')
]
