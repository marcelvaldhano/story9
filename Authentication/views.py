# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import loginForm
from django.contrib.auth import authenticate, login, logout


# Create your views here.
def index(request):
    form = loginForm(request.POST or None)
    flag = False
    if request.method == "POST":
        form = loginForm(request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            passw = form.cleaned_data['password']
            user = authenticate(request, username=uname, password=passw)
            if user is not None:
                login(request, user)
                flag = True
                response = {'form':form,'user':user,'flag':flag}
                return redirect('index')
            else:
                response = {'form':form,'user':user,'flag':flag}
                return redirect('index')
    else:
        response = {'form':form}
        return render(request,'index.html',response)

def logout_view(request):
    logout(request)
    form = loginForm()
    response = {'form':form}
    return redirect('')

def profile(request):
    return render(request, 'profile.html', {'user':request.user})