from django.test import TestCase, Client
import unittest
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.urls import resolve
from .views import index
import time

# Create your tests here.
class Test(TestCase):
    def test_url_index_exist(self):
        response=self.client.get('')
        self.assertEqual(response.status_code,200)
    
    def test_url_not_exist(self):
        response = self.client.get("marcelganteng")
        self.assertEqual(response.status_code, 404)
    
    def test_url_logout_exist(self):
        response=self.client.get('/logout/')
        self.assertEqual(response.status_code,200)

    def test_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response,'index.html')
            
    def test_using_landing_page_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response,'index.html')
        
    def test_check_profile_html(self):
        response =self.client.get('/profile/')
        self.assertTemplateUsed(response,'profile.html')
    
    def test_header(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Welcome",html_response)

   
